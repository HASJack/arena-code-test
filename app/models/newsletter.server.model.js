'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Newsletter Schema
 */
var NewsletterSchema = new Schema({
	postcode: {
		type: String,
		default: '',
		required: 'Please fill in your postcode',
		trim: true
	},
	email: {
		type: String,
		default: '',
		required: 'Please fill in your email',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	active: {
		type: Boolean,
		default: true
	}
});

mongoose.model('Newsletter', NewsletterSchema);