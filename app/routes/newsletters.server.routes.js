'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var newsletters = require('../../app/controllers/newsletters.server.controller');

	// Newsletters Routes
	app.route('/newsletters')
		.get(newsletters.list)
		// .post(users.requiresLogin, newsletters.create);
		.post(newsletters.create);

	app.route('/newsletters/:newsletterId')
		.get(newsletters.read)
		.put(users.requiresLogin, newsletters.hasAuthorization, newsletters.update)
		.delete(users.requiresLogin, newsletters.hasAuthorization, newsletters.delete);

	// Finish by binding the Newsletter middleware
	app.param('newsletterId', newsletters.newsletterByID);
};
