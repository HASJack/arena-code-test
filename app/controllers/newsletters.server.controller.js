'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Newsletter = mongoose.model('Newsletter'),
	_ = require('lodash'),
	config = require('../../config/config'),
	nodemailer = require('nodemailer'),
	async = require('async');

/**
 * Create a Newsletter
 */
exports.create = function(req, res, next) {
	var newsletter = new Newsletter(req.body);
	newsletter.user = req.user;

	newsletter.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(newsletter);
			async.waterfall([
				function(done) {
					res.render('templates/ultimate-guide-promo', 
					{
						server:'https://arena-code-test.herokuapp.com',
						url:'modules/newsletters/assets/UltimateGuide.pdf'
					},
					function(err, emailHTML) {
						done(err, emailHTML);
					});
				},
				// If valid email, send reset email using service
				function(emailHTML, done) {
					var smtpTransport = nodemailer.createTransport(config.mailer.options);
					var mailOptions = {
						to: newsletter.email,
						from: config.mailer.from,
						subject: 'The Ultimate Guide to Flowers',
						html: emailHTML
					};
					smtpTransport.sendMail(mailOptions, function(err) {
						if (!err) {
							res.send();
						}

						done(err);
					});
				}
			], function(err) {
				if (err) return next(err);
			});
		}
	});
};

/**
 * Show the current Newsletter
 */
exports.read = function(req, res) {
	res.jsonp(req.newsletter);
};

/**
 * Update a Newsletter
 */
exports.update = function(req, res) {
	var newsletter = req.newsletter ;

	newsletter = _.extend(newsletter , req.body);

	newsletter.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(newsletter);
		}
	});
};

/**
 * Delete an Newsletter
 */
exports.delete = function(req, res) {
	var newsletter = req.newsletter ;

	newsletter.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(newsletter);
		}
	});
};

/**
 * List of Newsletters
 */
exports.list = function(req, res) { 
	Newsletter.find().sort('-created').populate('user', 'displayName').exec(function(err, newsletters) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(newsletters);
		}
	});
};

/**
 * Newsletter middleware
 */
exports.newsletterByID = function(req, res, next, id) { 
	Newsletter.findById(id).populate('user', 'displayName').exec(function(err, newsletter) {
		if (err) return next(err);
		if (! newsletter) return next(new Error('Failed to load Newsletter ' + id));
		req.newsletter = newsletter ;
		next();
	});
};

/**
 * Newsletter authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.newsletter.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
