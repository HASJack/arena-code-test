'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Newsletter = mongoose.model('Newsletter'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, newsletter;

/**
 * Newsletter routes tests
 */
describe('Newsletter CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Newsletter
		user.save(function() {
			newsletter = {
				name: 'Newsletter Name'
			};

			done();
		});
	});

	it('should be able to save Newsletter instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Newsletter
				agent.post('/newsletters')
					.send(newsletter)
					.expect(200)
					.end(function(newsletterSaveErr, newsletterSaveRes) {
						// Handle Newsletter save error
						if (newsletterSaveErr) done(newsletterSaveErr);

						// Get a list of Newsletters
						agent.get('/newsletters')
							.end(function(newslettersGetErr, newslettersGetRes) {
								// Handle Newsletter save error
								if (newslettersGetErr) done(newslettersGetErr);

								// Get Newsletters list
								var newsletters = newslettersGetRes.body;

								// Set assertions
								(newsletters[0].user._id).should.equal(userId);
								(newsletters[0].name).should.match('Newsletter Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Newsletter instance if not logged in', function(done) {
		agent.post('/newsletters')
			.send(newsletter)
			.expect(401)
			.end(function(newsletterSaveErr, newsletterSaveRes) {
				// Call the assertion callback
				done(newsletterSaveErr);
			});
	});

	it('should not be able to save Newsletter instance if no name is provided', function(done) {
		// Invalidate name field
		newsletter.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Newsletter
				agent.post('/newsletters')
					.send(newsletter)
					.expect(400)
					.end(function(newsletterSaveErr, newsletterSaveRes) {
						// Set message assertion
						(newsletterSaveRes.body.message).should.match('Please fill Newsletter name');
						
						// Handle Newsletter save error
						done(newsletterSaveErr);
					});
			});
	});

	it('should be able to update Newsletter instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Newsletter
				agent.post('/newsletters')
					.send(newsletter)
					.expect(200)
					.end(function(newsletterSaveErr, newsletterSaveRes) {
						// Handle Newsletter save error
						if (newsletterSaveErr) done(newsletterSaveErr);

						// Update Newsletter name
						newsletter.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Newsletter
						agent.put('/newsletters/' + newsletterSaveRes.body._id)
							.send(newsletter)
							.expect(200)
							.end(function(newsletterUpdateErr, newsletterUpdateRes) {
								// Handle Newsletter update error
								if (newsletterUpdateErr) done(newsletterUpdateErr);

								// Set assertions
								(newsletterUpdateRes.body._id).should.equal(newsletterSaveRes.body._id);
								(newsletterUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Newsletters if not signed in', function(done) {
		// Create new Newsletter model instance
		var newsletterObj = new Newsletter(newsletter);

		// Save the Newsletter
		newsletterObj.save(function() {
			// Request Newsletters
			request(app).get('/newsletters')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Newsletter if not signed in', function(done) {
		// Create new Newsletter model instance
		var newsletterObj = new Newsletter(newsletter);

		// Save the Newsletter
		newsletterObj.save(function() {
			request(app).get('/newsletters/' + newsletterObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', newsletter.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Newsletter instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Newsletter
				agent.post('/newsletters')
					.send(newsletter)
					.expect(200)
					.end(function(newsletterSaveErr, newsletterSaveRes) {
						// Handle Newsletter save error
						if (newsletterSaveErr) done(newsletterSaveErr);

						// Delete existing Newsletter
						agent.delete('/newsletters/' + newsletterSaveRes.body._id)
							.send(newsletter)
							.expect(200)
							.end(function(newsletterDeleteErr, newsletterDeleteRes) {
								// Handle Newsletter error error
								if (newsletterDeleteErr) done(newsletterDeleteErr);

								// Set assertions
								(newsletterDeleteRes.body._id).should.equal(newsletterSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Newsletter instance if not signed in', function(done) {
		// Set Newsletter user 
		newsletter.user = user;

		// Create new Newsletter model instance
		var newsletterObj = new Newsletter(newsletter);

		// Save the Newsletter
		newsletterObj.save(function() {
			// Try deleting Newsletter
			request(app).delete('/newsletters/' + newsletterObj._id)
			.expect(401)
			.end(function(newsletterDeleteErr, newsletterDeleteRes) {
				// Set message assertion
				(newsletterDeleteRes.body.message).should.match('User is not logged in');

				// Handle Newsletter error error
				done(newsletterDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Newsletter.remove().exec();
		done();
	});
});