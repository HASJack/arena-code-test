'use strict';

//Setting up route
angular.module('newsletters').config(['$stateProvider',
	function($stateProvider) {
		// Newsletters state routing
		$stateProvider.
		state('listNewsletters', {
			url: '/newsletters',
			templateUrl: 'modules/newsletters/views/list-newsletters.client.view.html'
		}).
		state('createNewsletter', {
			url: '/newsletters/create',
			templateUrl: 'modules/newsletters/views/create-newsletter.client.view.html'
		}).
		state('viewNewsletter', {
			url: '/newsletters/:newsletterId',
			templateUrl: 'modules/newsletters/views/view-newsletter.client.view.html'
		}).
		state('editNewsletter', {
			url: '/newsletters/:newsletterId/edit',
			templateUrl: 'modules/newsletters/views/edit-newsletter.client.view.html'
		});
	}
]);