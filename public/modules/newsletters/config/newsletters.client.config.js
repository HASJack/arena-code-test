'use strict';

// Configuring the Articles module
angular.module('newsletters').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Newsletters', 'newsletters', 'dropdown', '/newsletters(/create)?');
		Menus.addSubMenuItem('topbar', 'newsletters', 'List Newsletters', 'newsletters');
		Menus.addSubMenuItem('topbar', 'newsletters', 'New Newsletter', 'newsletters/create');
	}
]);