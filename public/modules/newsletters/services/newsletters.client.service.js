'use strict';

//Newsletters service used to communicate Newsletters REST endpoints
angular.module('newsletters').factory('Newsletters', ['$resource',
	function($resource) {
		return $resource('newsletters/:newsletterId', { newsletterId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);