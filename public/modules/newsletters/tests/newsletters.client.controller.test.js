'use strict';

(function() {
	// Newsletters Controller Spec
	describe('Newsletters Controller Tests', function() {
		// Initialize global variables
		var NewslettersController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Newsletters controller.
			NewslettersController = $controller('NewslettersController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Newsletter object fetched from XHR', inject(function(Newsletters) {
			// Create sample Newsletter using the Newsletters service
			var sampleNewsletter = new Newsletters({
				name: 'New Newsletter'
			});

			// Create a sample Newsletters array that includes the new Newsletter
			var sampleNewsletters = [sampleNewsletter];

			// Set GET response
			$httpBackend.expectGET('newsletters').respond(sampleNewsletters);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.newsletters).toEqualData(sampleNewsletters);
		}));

		it('$scope.findOne() should create an array with one Newsletter object fetched from XHR using a newsletterId URL parameter', inject(function(Newsletters) {
			// Define a sample Newsletter object
			var sampleNewsletter = new Newsletters({
				name: 'New Newsletter'
			});

			// Set the URL parameter
			$stateParams.newsletterId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/newsletters\/([0-9a-fA-F]{24})$/).respond(sampleNewsletter);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.newsletter).toEqualData(sampleNewsletter);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Newsletters) {
			// Create a sample Newsletter object
			var sampleNewsletterPostData = new Newsletters({
				name: 'New Newsletter'
			});

			// Create a sample Newsletter response
			var sampleNewsletterResponse = new Newsletters({
				_id: '525cf20451979dea2c000001',
				name: 'New Newsletter'
			});

			// Fixture mock form input values
			scope.name = 'New Newsletter';

			// Set POST response
			$httpBackend.expectPOST('newsletters', sampleNewsletterPostData).respond(sampleNewsletterResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Newsletter was created
			expect($location.path()).toBe('/newsletters/' + sampleNewsletterResponse._id);
		}));

		it('$scope.update() should update a valid Newsletter', inject(function(Newsletters) {
			// Define a sample Newsletter put data
			var sampleNewsletterPutData = new Newsletters({
				_id: '525cf20451979dea2c000001',
				name: 'New Newsletter'
			});

			// Mock Newsletter in scope
			scope.newsletter = sampleNewsletterPutData;

			// Set PUT response
			$httpBackend.expectPUT(/newsletters\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/newsletters/' + sampleNewsletterPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid newsletterId and remove the Newsletter from the scope', inject(function(Newsletters) {
			// Create new Newsletter object
			var sampleNewsletter = new Newsletters({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Newsletters array and include the Newsletter
			scope.newsletters = [sampleNewsletter];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/newsletters\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleNewsletter);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.newsletters.length).toBe(0);
		}));
	});
}());