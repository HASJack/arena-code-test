'use strict';

// Newsletters controller
angular.module('newsletters').controller('NewslettersController', ['$scope','$window', '$stateParams', '$location', 'Authentication', 'Newsletters',
	function($scope, $window, $stateParams, $location, Authentication, Newsletters) {

		$scope.authentication = Authentication;

		// UK Postcode Regex: http://www.regxlib.com/REDetails.aspx?regexp_id=260
		$scope.UKGovPostcodeRegex = /^(GIR|[A-Z]\d[A-Z\d]??|[A-Z]{2}\d[A-Z\d]??)[ ]??(\d[A-Z]{2})$/;

		$scope.gohome = function() {
			$window.open('http://www.arenaflowers.com/');
		};

		$scope.showForm = true;


		// Create:
		$scope.create = function() {
			if ($scope.myForm.$valid) {
				// Hide the form
				$scope.showForm = false;
				// Show the progress indicator
				$scope.showIndicator = true;
				// Create new object
				var newsletter = new Newsletters ({
					email: this.email,
					postcode: this.postcode
				});

				// Show download on save
				newsletter.$save(function(response) {
					$scope.showIndicator = false;
					$scope.showDownload = true;

					// Clear form fields
					$scope.email = '';
					$scope.postcode = '';
					// Add download URL:
					$scope.guide = 'modules/newsletters/assets/UltimateGuide.pdf';
				}, function(errorResponse) {
					$scope.error = errorResponse.data.message;
				});
			}
			else {
				if ($scope.myForm.useremail.$error.required) {
					$scope.error = 'Please enter your email address';
				} else {
					if ($scope.myForm.useremail.$error.pattern) {
						$scope.error = 'Please enter a valid email address';
					} else {
						if ($scope.myForm.postcode.$error.required) {
							$scope.error = 'Please enter your postcode';
						}
						else {
							if ($scope.myForm.postcode.$error.pattern) {
								$scope.error = 'Please enter a valid postcode';
							}
						}
					}
				}
			}
		};

		// Remove existing Newsletter
		$scope.remove = function(newsletter) {
			if ( newsletter ) { 
				newsletter.$remove();

				for (var i in $scope.newsletters) {
					if ($scope.newsletters [i] === newsletter) {
						$scope.newsletters.splice(i, 1);
					}
				}
			} else {
				$scope.newsletter.$remove(function() {
					$location.path('newsletters');
				});
			}
		};

		// Update existing Newsletter
		$scope.update = function() {
			var newsletter = $scope.newsletter;

			newsletter.$update(function() {
				$location.path('newsletters/' + newsletter._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Newsletters
		$scope.find = function() {
			$scope.newsletters = Newsletters.query();
		};

		// Find existing Newsletter
		$scope.findOne = function() {
			$scope.newsletter = Newsletters.get({ 
				newsletterId: $stateParams.newsletterId
			});
		};
	}
]);